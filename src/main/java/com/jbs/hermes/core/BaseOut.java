package com.jbs.hermes.core;

/**
 * @Author LegendaryFlutist
 * @Description //base return
 * @Date 7:39 2019/5/23
 * @Param
 * @return
 **/
public class BaseOut {

    private int code = 2112;

    private String msg;

    private Object data;

    public BaseOut() {
    }

    public BaseOut(int code) {
        this.code = code;
    }

    public BaseOut(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BaseOut(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "BaseOut{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
