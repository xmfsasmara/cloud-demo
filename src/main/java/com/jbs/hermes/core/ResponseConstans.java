package com.jbs.hermes.core;

import com.alibaba.fastjson.JSON;

import com.jbs.hermes.constants.ErrContext;
import com.jbs.hermes.constants.ResultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseConstans {

    private static final Logger logger = LoggerFactory.getLogger(ResponseConstans.class);


    /**
     * send success response
     *
     * @param response
     * @param result
     */
    public static void responseResult(HttpServletResponse response, BaseOut result) {
        int status = HttpStatus.OK.value();

        addCors(response);
        response.setStatus(status);
        try {
            response.getWriter().write(JSON.toJSONString(result));
        } catch (IOException ex) {
            logger.error("responseResult error", ex);
        }
    }


    /**
     * send error response
     *
     * @param response
     * @param errContext
     */
    public static void responseErrResult(HttpServletResponse response, ErrContext errContext) {
        response.setStatus(errContext.getStatusCode());

        addCors(response);
        try {
            response.getWriter().write(JSON.toJSONString(errContext.getErr()));
        } catch (IOException ex) {
            logger.error("responseErrResult error", ex);
        }

    }

    private static void addCors(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
    }

    public static void responseResult(HttpServletResponse response, BaseOutRep result) {
        int status = HttpStatus.OK.value();
        addCors(response);

        String code = result.getCode();
        ResultCode resultCode = ResultCode.parseValue(code);
        if (resultCode != null) {
            status = resultCode.getHttpStatus().value();
        } else {
            status = HttpStatus.BAD_REQUEST.value();
        }
        response.setStatus(status);

        try {
            result.setCode("enterpriseapi." + code);
            response.getWriter().write(JSON.toJSONString(result));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
