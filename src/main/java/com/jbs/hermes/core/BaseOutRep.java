package com.jbs.hermes.core;

import com.alibaba.fastjson.JSON;
import com.jbs.hermes.constants.ResultCode;

/**
 * 统一API响应结果封装
 */
public class BaseOutRep<T> {
    private String code;
    private String message;
    private T data;

    public BaseOutRep setCode(ResultCode resultCode) {
        this.code = resultCode.code();
        return this;
    }

    public BaseOutRep setCode(String code) {
        this.code = code;
        return this;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public BaseOutRep setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public BaseOutRep setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
