package com.jbs.hermes.dto;

public class Test {

    String abc;


    String bca;


    public String getAbc() {
        return abc;
    }

    public void setAbc(String abc) {
        this.abc = abc;
    }

    public String getBca() {
        return bca;
    }

    public void setBca(String bca) {
        this.bca = bca;
    }

    @Override
    public String toString() {
        return "Test{" +
                "abc='" + abc + '\'' +
                ", bca='" + bca + '\'' +
                '}';
    }
}
