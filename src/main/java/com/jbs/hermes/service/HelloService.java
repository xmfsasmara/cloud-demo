package com.jbs.hermes.service;


import com.jbs.hermes.feign.client.Demo2ServerFeignClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    /**
     * hello test
     * @return
     */


    @Autowired
    private Demo2ServerFeignClient demo2ServerFeignClient;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public String hello(){
        logger.info("access demo1 HelloService");
        return demo2ServerFeignClient.hello();
    }



}
