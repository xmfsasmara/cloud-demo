package com.jbs.hermes.controller;

import com.jbs.hermes.constants.WorkflowErrConstants;
import com.jbs.hermes.core.BaseOut;
import com.jbs.hermes.core.ServiceException;
import com.jbs.hermes.dto.Test;
import com.jbs.hermes.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
public class helloController {


    @Autowired
    private HelloService helloService;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @RequestMapping(value = "/hello" , method = RequestMethod.GET)
    public String hello() {

        logger.info("access demo1 helloController");
        String result = helloService.hello();
        logger.info("return："+result);
        return result;
    }

    @RequestMapping("/home")
    public String home() {
        System.out.println("1happy");
        return null;
    }

    @RequestMapping(value = "/helloob" , method = RequestMethod.GET)
    public BaseOut helloob() {

        logger.info("access demo1 helloController");

        BaseOut baseOut = new BaseOut();
        Test test = new Test();
        test.setAbc("123");
        test.setBca("321");
        baseOut.setMsg("success");
        baseOut.setData(test);

        logger.info("return："+baseOut);
        return baseOut;
    }

    @RequestMapping(value = "/helloerror" , method = RequestMethod.GET)
    public String error() {
        throw  new ServiceException(WorkflowErrConstants.ParamCheckError);
    }



}
