package com.jbs.hermes.interceptor;

import com.jbs.hermes.util.XcUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        System.out.println("IP拦截：" + XcUtil.getIpAddr(request) + "--" + request.getRequestURI());

        return super.preHandle(request, response, handler);
    }


}
