package com.jbs.hermes;


import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.cloud.client.discovery.DiscoveryClient;
@EnableDiscoveryClient
@RestController
@SpringBootApplication
@EnableFeignClients
@NacosPropertySource(dataId = "hermes", autoRefreshed = true)
public class HermesApplication {

    @Autowired
    private DiscoveryClient discoveryClient;


    /**
     * 获取所有服务
     */
    @RequestMapping("/services")
    public Object services() {
        return discoveryClient.getServices();
    }

    
    public static void main(String[] args) {
        SpringApplication.run(HermesApplication.class, args);
    }



}
