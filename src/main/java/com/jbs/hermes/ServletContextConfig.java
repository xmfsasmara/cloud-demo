package com.jbs.hermes;


import com.jbs.hermes.constants.ResultCode;
import com.jbs.hermes.core.BaseOutRep;
import com.jbs.hermes.core.ParamValidationResult;
import com.jbs.hermes.core.ResponseConstans;
import com.jbs.hermes.core.ServiceException;
import com.jbs.hermes.interceptor.LoginInterceptor;
import com.google.common.collect.Lists;
import javafx.application.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.config.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by joshua on 2017/6/22.
 */
@Configuration
@ComponentScan(basePackageClasses = Application.class, useDefaultFilters = true)
public class ServletContextConfig extends WebMvcConfigurationSupport {

    private final Logger logger = LoggerFactory.getLogger(ServletContextConfig.class);

    @Autowired
    private LoginInterceptor loginInterceptor;

    /**
     * 配置servlet处理
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "OPTIONS", "PUT")
                .maxAge(3600);
    }



    /*@Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        System.out.println("Configure Message Converters");
        converters.add(new StringHttpMessageConverter());
        super.configureMessageConverters(converters);
    }*/

    /**
     * 统一异常处理
     *
     * @param exceptionResolvers
     */
    @Override
    public void configureHandlerExceptionResolvers(List<HandlerExceptionResolver> exceptionResolvers) {
        exceptionResolvers.add(new HandlerExceptionResolver() {
            public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
                String requestUri = request.getRequestURI();
                BaseOutRep result = new BaseOutRep();
                String message = null;
                if (e instanceof ServiceException) {//业务失败的异常，如“账号或密码错误”
                    result.setCode(((ServiceException) e).getErrContext().getErr().getCode())
                            .setMessage(((ServiceException) e).getErrContext().getErr().getMessage());
                    ResponseConstans.responseErrResult(response, ((ServiceException) e).getErrContext());
                    message = result.getMessage();
                    logger.error(message, e);
                    return new ModelAndView();
                } else if (e instanceof NoHandlerFoundException) {
                    result.setCode(ResultCode.AUTH_FAILD).setMessage(e.getMessage());
                } else if (e instanceof MethodArgumentNotValidException) {
                    List<ParamValidationResult> paramValidationResults = Lists.newArrayList();
                    for (FieldError error : ((MethodArgumentNotValidException) e).getBindingResult().getFieldErrors()) {
                        ParamValidationResult paramValidationResult = new ParamValidationResult();
                        paramValidationResult.setMessage(error.getDefaultMessage());
                        paramValidationResult.setParam(error.getField());
                        paramValidationResults.add(paramValidationResult);
                    }
                    result.setCode(ResultCode.INVALID_PARAM).setMessage("param error").setData(paramValidationResults);
                } else {
                    result.setCode(ResultCode.SERVICE_ERROR).setMessage(request.getRequestURI() + " error");
                }
                message = result.getMessage();
                logger.error(message, e);
                ResponseConstans.responseResult(response, result);
                return new ModelAndView();
            }
        });
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String[] excludePathPatterns = {
                "/actuator/health",
        };

        registry.addInterceptor(loginInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(excludePathPatterns);
    }


}