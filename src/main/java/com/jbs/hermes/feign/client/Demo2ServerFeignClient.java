
package com.jbs.hermes.feign.client;

import com.jbs.hermes.config.FeignLogConfiguration;
import com.jbs.hermes.feign.hystrix.Demo2ServerFeignClientHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "crm2", configuration = FeignLogConfiguration.class, fallbackFactory = Demo2ServerFeignClientHystrix.class)
public interface Demo2ServerFeignClient {


    @GetMapping("/home")
    String hello();



}

