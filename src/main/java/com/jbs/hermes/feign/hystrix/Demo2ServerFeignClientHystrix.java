package com.jbs.hermes.feign.hystrix;

import com.jbs.hermes.feign.client.Demo2ServerFeignClient;
import com.jbs.hermes.feign.factory.Demo2FeignClientWithFactory;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Demo2ServerFeignClientHystrix  implements FallbackFactory<Demo2ServerFeignClient> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Demo2ServerFeignClient.class);

    @Override
    public Demo2ServerFeignClient create(Throwable cause) {
        LOGGER.info("fallback; reason was: {}", cause.getMessage());
        return new Demo2FeignClientWithFactory() {

            @Override
            public String hello() {
                return null;
            }
        };
    }
}
