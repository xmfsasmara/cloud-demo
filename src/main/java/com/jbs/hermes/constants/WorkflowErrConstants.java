package com.jbs.hermes.constants;

import org.springframework.http.HttpStatus;

/**
 * @Author LegendaryFlutist
 * @Description //工作流异常定义
 * @Date 13:03 2019/5/22
 * @Param
 * @return
 **/
public class WorkflowErrConstants extends ErrConstants {

    public static final ErrContext ParamCheckError = new ErrContext(HttpStatus.INTERNAL_SERVER_ERROR.value(),
            "Joybos.535001", "Param check error");


}
